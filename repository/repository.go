package repository

import (
	"context"
	"weconnect/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type IRepository interface {
	Ping(ctx context.Context) error
	InsertBatch(ctx context.Context, input []interface{}) error
	Find(ctx context.Context, page, limit int) ([]models.Data, error)
}

var Repository *repository

type repository struct {
	*models.Models
}

func NewRepository(m *models.Models) {
	Repository = &repository{
		Models: m,
	}
}

func GetRepository() *repository {
	return Repository
}

func (r *repository) Ping(ctx context.Context) error {
	client, err := r.Models.GetClient()
	if err != nil {
		return err
	}
	return client.Ping(ctx, nil)
}

func (r *repository) InsertBatch(ctx context.Context, input []interface{}) error {
	_, err := r.Models.CollWeconnect().InsertMany(ctx, input)
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) Find(ctx context.Context, page, limit int) ([]models.Data, error) {
	filter := bson.D{{}}
	options := new(options.FindOptions)
	if limit != 0 {
		if page == 0 {
			page = 1
		}
		options.SetSkip(int64((page - 1) * limit))
		options.SetLimit(int64(limit))
	}
	cursor, err := r.Models.CollWeconnect().Find(ctx, filter, options)
	if err != nil {
		return nil, err
	}

	var data []models.Data
	if err = cursor.All(ctx, &data); err != nil {
		panic(err)
	}
	return data, nil
}
