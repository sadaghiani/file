package data

import (
	"context"
	"weconnect/models"
)

func (d *Data) Get(ctx context.Context, page, limit int) ([]models.Data, error) {

	data, err := d.IRepository.Find(ctx, page, limit)
	if err != nil {
		return nil, err
	}
	return data, nil
}
