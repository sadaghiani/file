package data

import (
	"context"
	"weconnect/models"
	"weconnect/repository"
)

type IData interface {
	Get(ctx context.Context) ([]models.Data, error)
}

type Data struct {
	repository.IRepository
}

func NewData() *Data {
	return &Data{
		IRepository: repository.GetRepository(),
	}
}
