package file

import (
	"context"
	"mime/multipart"
	"weconnect/repository"
)

type IFile interface {
	Upload(ctx context.Context, ff *multipart.FileHeader) error
}

type File struct {
	repository.IRepository
}

func NewFile() *File {
	return &File{
		IRepository: repository.GetRepository(),
	}
}
