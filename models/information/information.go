package information

import (
	"context"
	"weconnect/repository"
)

type IInformation interface {
	Health(ctx context.Context) error
}

type Information struct {
	repository.IRepository
}

func NewInformation() *Information {
	return &Information{
		IRepository: repository.GetRepository(),
	}
}
