package models

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"weconnect/pkg/config"
	"weconnect/pkg/logger"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

type IMongoDatastore interface {
	GetDatabase() *mongo.Database
	GetClient() (*mongo.Client, error)
	CollWeconnect(opts ...*options.CollectionOptions) *mongo.Collection
	Disconnect() error
}

func (md *MongoDataStore) GetDatabase() *mongo.Database {
	return md.database
}

func (md *MongoDataStore) GetClient() (*mongo.Client, error) {
	if md.client != nil {
		return md.client, nil
	}
	return nil, errors.New("client is missing (nil) in Mongo Data Store")
}

func (md *MongoDataStore) CollWeconnect(opts ...*options.CollectionOptions) *mongo.Collection {
	return md.database.Collection(config.GetENV(config.DB_COLLECTION), opts...)
}

func (md *MongoDataStore) Disconnect() error {
	err := md.client.Disconnect(context.Background())
	if err != nil {
		return err
	}
	return nil
}

type MongoDataStore struct {
	database *mongo.Database
	client   *mongo.Client
}

func NewDatastore() *MongoDataStore {

	database, client := connect()

	if database != nil && client != nil {
		mongoDataStore := new(MongoDataStore)
		mongoDataStore.database = database
		mongoDataStore.client = client
		return mongoDataStore
	}
	logger.Log.Fatal("Failed to connect to database")

	return nil
}

func connect() (*mongo.Database, *mongo.Client) {

	var connectOnce sync.Once
	var database *mongo.Database
	var client *mongo.Client

	connectOnce.Do(func() {
		database, client = connectToMongo()
	})

	return database, client
}

func connectToMongo() (*mongo.Database, *mongo.Client) {

	var err error
	client, err := mongo.NewClient(newClientOption())
	if err != nil {
		logger.Log.Fatal("cannot create newClient", zap.Error(err))
	}
	client.Connect(context.TODO())
	if err != nil {
		logger.Log.Fatal("cannot client connect", zap.Error(err))
	}

	var database = client.Database(config.GetENV(config.DB_NAME))
	logger.Log.Info("conncected to database")

	return database, client
}

func newClientOption() *options.ClientOptions {

	credential := options.Credential{
		AuthMechanism: "SCRAM-SHA-256",
		Username:      config.GetENV(config.DB_USER),
		Password:      config.GetENV(config.DB_PASSWORD),
	}

	uri := fmt.Sprintf("mongodb://%s:%s/", config.GetENV(config.DB_URI), config.GetENV(config.DB_PORT))
	return options.Client().ApplyURI(uri).SetAuth(credential)
}
