package models

const (
	ErrorStatus   = "error"
	SuccessStatus = "success"
)

type Models struct {
	IMongoDatastore
}

type GeneralResponse struct {
	Status  string      `json:"status"`
	Code    uint        `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

func NewModels() *Models {

	return &Models{
		IMongoDatastore: NewDatastore(),
	}
}
