package config

import (
	"fmt"
	"log"
)

type ENV string

const (
	LOG_LEVEL      ENV = "LOG_LEVEL"
	BATCH_SIZE     ENV = "BATCH_SIZE"
	NUMBER_WORKERS ENV = "NUMBER_WORKERS"
	DB_URI         ENV = "DB_URI"
	DB_PORT        ENV = "DB_PORT"
	DB_NAME        ENV = "DB_NAME"
	DB_COLLECTION  ENV = "DB_COLLECTION"
	DB_USER        ENV = "DB_USER"
	DB_PASSWORD    ENV = "DB_PASSWORD"
)

func initEnv() {
	envs := []ENV{
		LOG_LEVEL,
		BATCH_SIZE,
		NUMBER_WORKERS,
		DB_URI,
		DB_PORT,
		DB_NAME,
		DB_COLLECTION,
		DB_USER,
		DB_PASSWORD,
	}
	bindEnvs(envs...)
	loadEnvs(envs...)
}

func bindEnvs(envs ...ENV) {

	var errs []error
	for _, v := range envs {
		err := config.BindEnv(string(v))
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) != 0 {
		log.Fatal("error :", errs)
	}
}

func loadEnvs(envs ...ENV) {

	var errs []error
	for _, v := range envs {
		err := loadEnv(v)
		if err != nil {
			errs = append(errs, err)
		}
	}

	if len(errs) != 0 {
		log.Fatal(errs, " must declare in os environment")
	}
}

func loadEnv(env ENV) error {

	if config.GetString(string(env)) == "" {
		return fmt.Errorf(string(env))
	}

	return nil
}

func GetENV(e ENV) string {
	return config.GetString(string(e))
}
