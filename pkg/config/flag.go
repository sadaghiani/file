package config

import (
	"flag"

	"github.com/spf13/pflag"
)

func initFlag() {

	flag.String("port", "80", "http listener port")
	flag.String("host", "", "http listener address")

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	flag.Parse()

	config.BindPFlags(pflag.CommandLine)
}
