package config

import (
	"github.com/spf13/viper"
)

var config *viper.Viper

func init() {

	config = viper.New()

	initEnv()
	initFlag()

}

func GetConfig() *viper.Viper {
	return config
}
