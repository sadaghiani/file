package server

import (
	"log"
	"weconnect/pkg/config"
	"weconnect/routers"
)

func Run(r *routers.Routes) {

	host := config.GetConfig().GetString("host")
	port := config.GetConfig().GetString("port")

	log.Fatalln(r.Run(host + ":" + port))
}
