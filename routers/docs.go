package routers

import (
	"weconnect/docs"

	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func (r *Routes) docs() {

	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Title = "weconnect"
	docs.SwaggerInfo.Version = "0.0.0"

	r.routers.GET("/docs/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
}
