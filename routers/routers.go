package routers

import (
	"time"
	"weconnect/controlers"
	"weconnect/pkg/config"
	"weconnect/pkg/logger"

	"github.com/gin-gonic/gin"

	ginzap "github.com/gin-contrib/zap"
)

type Routes struct {
	routers    *gin.Engine
	controlers *controlers.Controlers
}

func NewRouter(controlers *controlers.Controlers) *Routes {

	r := Routes{
		routers:    gin.New(),
		controlers: controlers,
	}

	r.routers.Use(config.CORS())
	r.routers.Use(config.FixOptionMethod())

	r.routers.Use(ginzap.Ginzap(logger.Log, time.RFC3339, true))
	r.routers.Use(ginzap.RecoveryWithZap(logger.Log, true))

	v1 := r.routers.Group("/api/v1")

	r.root()
	r.docs()
	r.file(v1)
	r.data(v1)
	r.health(v1)

	return &r
}

func (r Routes) Run(addr ...string) error {
	return r.routers.Run(addr...)
}
