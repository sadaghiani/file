package main

import (
	"time"
	"weconnect/controlers"
	"weconnect/models"
	"weconnect/pkg/config"
	"weconnect/pkg/logger"
	"weconnect/repository"
	"weconnect/routers"
	"weconnect/server"
)

func init() {
	logger.NewLogger(config.GetENV(config.LOG_LEVEL), time.RFC3339)
}

func main() {

	m := models.NewModels()
	repository.NewRepository(m)
	c := controlers.NewControlers(m)
	r := routers.NewRouter(c)
	server.Run(r)
}
