package data

import (
	"weconnect/models/data"
)

type Data struct {
	data *data.Data
}

func NewData() *Data {
	return &Data{
		data: data.NewData(),
	}
}
