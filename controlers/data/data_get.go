package data

import (
	"context"
	"fmt"
	"net/http"
	"weconnect/models"

	"github.com/gin-gonic/gin"
)

type Query struct {
	Page  int `json:"page" binding:"required"`
	Limit int `json:"limit" binding:"required"`
}

// @Summary csv data
// @Description csv data
// @Tags data
// @Param Page query integer true " "
// @Param Limit query integer true " "
// @Accept json
// @Produce json
// @Success 200 {object} models.GeneralResponse
// @Success 400 {object} models.GeneralResponse
// @Router /data/ [get]
func (d *Data) Get(c *gin.Context) {

	var q Query
	if err := c.BindQuery(&q); err != nil {
		c.JSON(http.StatusPreconditionRequired, models.GeneralResponse{
			Status:  models.ErrorStatus,
			Code:    http.StatusPreconditionRequired,
			Message: err.Error(),
		},
		)
		return
	}

	data, err := d.data.Get(context.Background(), q.Page, q.Limit)

	defer func() {
		if r := recover(); r != nil {
			c.JSON(http.StatusInternalServerError, models.GeneralResponse{
				Status:  models.ErrorStatus,
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintln(r),
			},
			)
			return
		}
	}()

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.GeneralResponse{
			Status:  models.ErrorStatus,
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		},
		)
	} else {
		c.JSON(http.StatusOK, models.GeneralResponse{
			Status:  models.SuccessStatus,
			Code:    http.StatusOK,
			Message: "get data successful",
			Data:    data,
		},
		)
	}
}
