package information

import (
	"context"
	"fmt"
	"net/http"
	"weconnect/models"

	"github.com/gin-gonic/gin"
)

// @Summary healthcheck
// @Description healthcheck
// @Tags information
// @Accept json
// @Produce json
// @Success 200 {object} models.GeneralResponse
// @Success 400 {object} models.GeneralResponse
// @Router /health [get]
func (i *Information) Health(c *gin.Context) {

	err := i.information.Health(context.Background())

	defer func() {
		if r := recover(); r != nil {
			c.JSON(http.StatusInternalServerError, models.GeneralResponse{
				Status:  models.ErrorStatus,
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintln(r),
			},
			)
			return
		}
	}()

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.GeneralResponse{
			Status:  models.ErrorStatus,
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		},
		)
	} else {
		c.JSON(http.StatusOK, models.GeneralResponse{
			Status:  models.SuccessStatus,
			Code:    http.StatusOK,
			Message: "service healthy",
		})
	}

}
