package information

import (
	"weconnect/models/information"
)

type Information struct {
	information *information.Information
}

func NewInformation() *Information {
	return &Information{
		information: information.NewInformation(),
	}
}
