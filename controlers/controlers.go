package controlers

import (
	"weconnect/controlers/data"
	"weconnect/controlers/file"
	"weconnect/controlers/information"
	"weconnect/models"
)

type Controlers struct {
	Models      *models.Models
	Information *information.Information
	File        *file.File
	Data        *data.Data
}

func NewControlers(models *models.Models) *Controlers {
	return &Controlers{
		Models:      models,
		Information: information.NewInformation(),
		File:        file.NewFile(),
		Data:        data.NewData(),
	}
}
