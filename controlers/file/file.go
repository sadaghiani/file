package file

import (
	"weconnect/models/file"
)

type File struct {
	file *file.File
}

func NewFile() *File {
	return &File{
		file: file.NewFile(),
	}
}
