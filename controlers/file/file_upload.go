package file

import (
	"context"
	"fmt"
	"net/http"
	"weconnect/models"

	"github.com/gin-gonic/gin"
)

// @Summary upload csv file
// @Description upload csv file
// @Tags file
// @Param file formData  file true " "
// @Accept mpfd
// @Produce json
// @Success 200 {object} models.GeneralResponse
// @Success 400 {object} models.GeneralResponse
// @Router /file/upload [post]
func (f *File) Upload(c *gin.Context) {

	newfile, err := c.FormFile("file")

	defer func() {
		if r := recover(); r != nil {
			c.JSON(http.StatusInternalServerError, models.GeneralResponse{
				Status:  models.ErrorStatus,
				Code:    http.StatusInternalServerError,
				Message: fmt.Sprintln(r),
			},
			)
			return
		}
	}()

	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, models.GeneralResponse{
			Status:  models.ErrorStatus,
			Code:    http.StatusUnprocessableEntity,
			Message: err.Error(),
		},
		)
		return
	}

	err = f.file.Upload(context.Background(), newfile)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.GeneralResponse{
			Status:  models.ErrorStatus,
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		},
		)
	} else {
		c.JSON(http.StatusOK, models.GeneralResponse{
			Status:  models.SuccessStatus,
			Code:    http.StatusOK,
			Message: "upload successful",
		},
		)
	}
}
